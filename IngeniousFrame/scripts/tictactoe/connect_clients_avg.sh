#Vanilla
for i in `seq 1 $1`
do
  # creates a new tictactoe game with the default configuration
  java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "TicTacToe.json" -game "TTTReferee" -lobby "mylobby" -port $4
  java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "$2_player1" -config "EnhancementChoices/EnhancementChoiceVanilla.json" -engine "za.ac.sun.cs.ingenious.games.tictactoe.engines.TreeEngine.TTTMCTSTreeGenericEngine" -threadCount 1 -game "TTTReferee" -hostname localhost -port $4 &
  java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "$3_player2" -config "EnhancementChoices/EnhancementChoiceVanilla.json" -engine "za.ac.sun.cs.ingenious.games.tictactoe.engines.TreeEngine.TTTMCTSTreeGenericEngine" -threadCount 1 -game "TTTReferee" -hostname 127.0.0.1 -port $4
  echo "-" -n
  cp averagePlayoutsPerSecond.txt first$i.txt
done
  cat first1.txt first2.txt first3.txt first4.txt > averagePlayoutsPerSecondVanillaSerial.txt
  rm first1.txt
  rm first2.txt
  rm first3.txt
  rm first4.txt

