#!/bin/bash

# creates a new tictactoe game with the default configuration
java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "TicTacToe.json" -game "shadowtictactoe" -lobby "mylobby" -players 2
