package za.co.percipio.mpl.exception;


public class MessageDeserializationException extends RuntimeException{
    private static final long serialVersionUID = -6323085790773077824L;

    public MessageDeserializationException(Exception e) {
        super("Message object could not be deserialized.", e);
    }
}
