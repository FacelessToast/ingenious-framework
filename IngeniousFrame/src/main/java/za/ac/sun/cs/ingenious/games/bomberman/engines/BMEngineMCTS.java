package za.ac.sun.cs.ingenious.games.bomberman.engines;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.time.Clock;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.bomberman.BMEngine;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMFinalEvaluator;
import za.ac.sun.cs.ingenious.games.bomberman.mcts.MCTSRandomPlayer;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMGenActionMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.NextRoundMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.UpdateBombsMessage;

/**
 * This engine uses Monte Carlo evaluation of a single ply search using random playouts.
 */
// TODO (see issue 154): Replace this using a regular single ply search, provided with a generic
// Monte Carlo evaluation function which takes a random playout policy.
// Also rename the class 
public class BMEngineMCTS extends BMEngine {

    public static final int DEFAULT_NUM_SIMULATIONS = 3;
    private int numSimulations;
    private BMFinalEvaluator eval = new BMFinalEvaluator();
    
	public BMEngineMCTS(EngineToServerConnection toServer) {
        this(toServer, DEFAULT_NUM_SIMULATIONS);
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	public BMEngineMCTS(EngineToServerConnection toServer, int numSimulations) {
		super(toServer);
        this.numSimulations = numSimulations;
		System.out.println("DFKDJFJ");
	}
	
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		BMGenActionMessage bmGenMove = (BMGenActionMessage) a; 
		return new PlayActionMessage(FlatMCTSGenMove(bmGenMove.getPlayerID(), bmGenMove.getRound(), a.getClock()));
	}
	
	@Override
	public String engineName() {
		return "BMEngineMCTS";
	}
	
	public Action MCTSGenMove(int id, int round){
		// TODO Add proper MCTS for Bomberman, see issue #238 
		//BMNode node = new BMNode((BMBoard) board.deepCopy(), (short)id);
	    //Action bestMove = MCTS.generateAction(node, new BMRandomPolicy(), new SingleValueUCT(), new BMUpdater(), Constants.TURN_LENGTH);
	    Action bestMove = null;
		return bestMove;
	}
	
	public Action FlatMCTSGenMove(int id, int round, Clock clock) {
		board.printPretty();
		Log.info("Round: "+round+" player "+id);
		List<Action> actions = logic.generateActions(board, id);
		Collections.shuffle(actions);
		Action bestMove = null;
		double maxSurvival = Double.NEGATIVE_INFINITY;
        for (Action action: actions) {
			double survivals = 0;
			for(int simulationNr = 0; simulationNr < numSimulations; simulationNr++) {
                int numPlayers = board.getPlayers().length;
				MCTSRandomPlayer[] MCTSRandomPlayers = new MCTSRandomPlayer[numPlayers];
				BMBoard newboard = (BMBoard) board.deepCopy();
				for (int i = 0; i < numPlayers; i++){
					MCTSRandomPlayers[i] = new MCTSRandomPlayer(i, newboard);
					if (i == id) {
						MCTSRandomPlayers[i].setFirstMove(action);
					}
				}
				boolean firstTime = true;
				int simRoundNr = 0;
				while(!logic.isTerminal(newboard) && simRoundNr < numSimulations){
					playRound(MCTSRandomPlayers, newboard, firstTime);
					firstTime = false;
					simRoundNr++;
				}
				survivals += eval.getScore(newboard)[id];
				if (clock.getRemainingTimeMS() < 100) {
					break;
				}
			}
			Log.info("	"+action.toString() + ": "+survivals);
			if(survivals > maxSurvival){
				bestMove = action;
				maxSurvival = survivals;
			}
			if (clock.getRemainingTimeMS() < 100) {
				break;
			}
		}
        System.out.println(bestMove);
		return bestMove;
	}
	
    private void playRound(MCTSRandomPlayer[] players, BMBoard board, boolean skipBombUpdate) {
        if (!skipBombUpdate) {
            //Update bombs and release resulting upgrades
        	logic.applyPlayedMoveMessage(new UpdateBombsMessage(-1, true), board);
        }
        
        List<PlayActionMessage> messages = new ArrayList<PlayActionMessage>();
        for (MCTSRandomPlayer player : players) {
        	if (player.isAlive()) {
        		messages.add(player.genAction(null));
        	}
        }
        Collections.shuffle(messages);
        for (PlayActionMessage data : messages) {
            Action generatedMove = (data == null ? null : data.getAction());
            if (!(generatedMove == null) || (!logic.validMove(board, generatedMove))) {
            	logic.applyPlayedMoveMessage(new PlayedMoveMessage(data), board);
            }
        }
        logic.applyPlayedMoveMessage(new NextRoundMessage(), board);
    }
}
