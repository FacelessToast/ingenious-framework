package za.ac.sun.cs.ingenious.games.cardGames.core.exceptions;

/**
 * The LocationNotFoundException.
 * 
 * Will be raised if a certain location in the value of a TreeMap-entry (which is an ArrayList of locations) could not be found.
 * 
 * @author Joshua Wiebe
 */
public class LocationNotFoundException extends Exception{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 327482937429L;
	
	/**
	 * Instantiates a new location not found exception.
	 *
	 * @param message the message
	 */
	public LocationNotFoundException(String message){
		super(message);
	}
	
}
