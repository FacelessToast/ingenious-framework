package za.ac.sun.cs.ingenious.games.mnk.engines;

import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.games.mnk.MNKEngine;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSDescender;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSNode;
import za.ac.sun.cs.ingenious.search.mcts.legacy.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.legacy.SimpleUpdater;
import za.ac.sun.cs.ingenious.search.mcts.legacy.UCT;

public class MNKMCTSEngine extends MNKEngine {

	public MNKMCTSEngine(EngineToServerConnection toServer) {
		super(toServer);
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	@Override
	public String engineName() {
		return "MNKMCTSEngine";
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		MCTSNode<MNKState> root = new MCTSNode<MNKState>(board, logic, null, null);
		return new PlayActionMessage(MCTS.generateAction(root,
				new RandomPolicy<MNKState>(logic, eval, new PerfectInformationActionSensor<>(), false),
				new MCTSDescender<MNKState>(logic, new UCT<>(), new PerfectInformationActionSensor<>()), 
				new SimpleUpdater<MCTSNode<MNKState>>(),
				Constants.TURN_LENGTH));
	}

}
