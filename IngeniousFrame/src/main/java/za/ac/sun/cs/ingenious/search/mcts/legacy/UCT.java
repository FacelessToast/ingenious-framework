package za.ac.sun.cs.ingenious.search.mcts.legacy;

import static java.lang.Math.sqrt;

import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;

/**
 * Uses the UCT formula to calculate a search value.
 * 
 * @author steve
 * @author Michael Krause
 */
public class UCT<S extends TurnBasedGameState, N extends SearchNode<S,N>> implements SearchValue<S,N> {

	private final double C;
	
	/**
	 * @param c The constant in the UCT search value formula
	 */
	public UCT(double c) {
		C = c;
	}

	/**
	 * Sets C to a commonly used value near 0.7
	 */
	public UCT() {
		this(1.0/sqrt(2.0));
	}
	
	@Override
	public double get(N node, int forPlayerID, int totalNofPlayouts) {
		double parentPlayouts = node.getParent().getVisits();	
		
		double barX = node.getReward()[forPlayerID]/node.getVisits();
		//double C = 1.0/((double)totalNofPlayouts/50.0);
		
		double uncertainty  = 2.0*C*sqrt(2.0*Math.log(parentPlayouts)/node.getVisits());
		
		return barX+uncertainty;
	}
}
