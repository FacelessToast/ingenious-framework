package za.ac.sun.cs.ingenious.core.configuration;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import za.ac.sun.cs.ingenious.core.exception.BadMatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;

/**
 * Objects of this class represent the settings for one instance of some game. The class supports
 * reading and modifying these settings for different setting types (int, boolean, ...).
 * The available settings will vary from game to game, but three settings are fixed:
 * LobbyName, numPlayers and gameName (used with the RefereeFactory to create a referee object)
 */
public final class MatchSetting implements Serializable {
    /**
     * Determines if a de-serialized file is compatible with this class.
     */
    private static final long serialVersionUID      = -3834774530442103753L;
    public static final  int  UNSET_POSITIVE_SCALAR = -1;
    private static final Gson gson = new Gson();

    private Map<String, Object> data;
    
    /**
     * Load a JSON file containing match settings
     * @param pathToMatchFile Complete path to a JSON file containing the match settings
     * @throws IOException if file could not be opened
     */
    public MatchSetting(String pathToMatchFile) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(pathToMatchFile));
        String jsonObject = new String(encoded, StandardCharsets.UTF_8);
        Object o = gson.fromJson(jsonObject, Object.class);
        data = (Map<String, Object>) o;
    }
    
    /**
     * Read match settings from the given JSON object
     * @param matchSettings JSON object containing the match settings
     */
    public MatchSetting(JsonElement matchSettings) {
        Object o = gson.fromJson(matchSettings, Object.class);
        data = (Map<String, Object>) o;
	}

    /**
     * @return Value of setting "LobbyName" or "" if absent
     */
    public String getLobbyName() {
    	try {
			return getSettingAsString("LobbyName");
		} catch (MissingSettingException | IncorrectSettingTypeException e) {
			return "";
		}
    }
    
    public void setLobbyName(String newLobbyName) {
		data.put("LobbyName", newLobbyName);
    }

    /**
     * @return Value of setting "numPlayers" or MatchSetting.UNSET_POSITIVE_SCALAR if absent
     */
    public int getNumPlayers() {
    	try {
			return getSettingAsInt("numPlayers");
		} catch (MissingSettingException | IncorrectSettingTypeException e) {
			return UNSET_POSITIVE_SCALAR;
		}
    }
    
    public void setNumPlayers(int newNumPlayers) {
		data.put("numPlayers", new Double(newNumPlayers));
    }

    /**
     * @return Value of setting "gameName" or "" if absent
     */
    public String getGameName() {
    	try {
			return getSettingAsString("gameName");
		} catch (MissingSettingException | IncorrectSettingTypeException e) {
			return "";
		}
    }
    
    public void setGameName(String newGameName) {
		data.put("gameName", newGameName);
    }
    
    private Object getSetting(String settingName) throws MissingSettingException {
    	if (!data.containsKey(settingName)) {
        	throw new MissingSettingException(settingName);
		}
        return data.get(settingName);
    }
    
    /**
     * @param settingName Key of the setting to read.
     * @return Value of the setting with key settingName as a String, if it exists.
     * @throws MissingSettingException If no setting with the given key exists.
     * @throws IncorrectSettingTypeException If a setting with settingName exists but is not of type String.
     */
    public String getSettingAsString(String settingName) throws MissingSettingException, IncorrectSettingTypeException {
        Object val = getSetting(settingName);
        if (!(val instanceof String)) {
        	throw new IncorrectSettingTypeException(settingName, "String");
        }
        return (String) val;
    }
    
    /**
     * @param settingName Key of the setting to read.
     * @param altValue Value to return if the given setting does not exist or is of the wrong type.
     * @return Value of the setting with key settingName as a String, if it exists, else altValue.
     */
    public String getSettingAsString(String settingName, String altValue) {
    	String ret;
    	try {
    		ret = getSettingAsString(settingName);
    	} catch (MissingSettingException | IncorrectSettingTypeException e) {
    		ret = altValue;
    	}    	
    	return ret;
    }

    /**
     * @param settingName Key of the setting to read.
     * @return Value of the setting with key settingName as a double, if it exists.
     * @throws MissingSettingException If no setting with the given key exists.
     * @throws IncorrectSettingTypeException If a setting with settingName exists but is not of type double.
     */
    public double getSettingAsDouble(String settingName) throws MissingSettingException, IncorrectSettingTypeException {
    	Object val = getSetting(settingName);
        if (!(val instanceof Double)) {
        	throw new IncorrectSettingTypeException(settingName, "Double");
        }
        return ((Double) val).doubleValue();
    }

    /**
     * @param settingName Key of the setting to read.
     * @param altValue Value to return if the given setting does not exist or is of the wrong type.
     * @return Value of the setting with key settingName as a double, if it exists, else altValue.
     */
    public double getSettingAsDouble(String settingName, double altValue) {
    	double ret;
    	try {
    		ret = getSettingAsDouble(settingName);
    	} catch (MissingSettingException | IncorrectSettingTypeException e) {
    		ret = altValue;
    	}    	
    	return ret;
    }

    /**
     * @param settingName Key of the setting to read.
     * @return Value of the setting with key settingName as a int, if it exists.
     * @throws MissingSettingException If no setting with the given key exists.
     * @throws IncorrectSettingTypeException If a setting with settingName exists but is not of type int.
     */
    public int getSettingAsInt(String settingName) throws MissingSettingException, IncorrectSettingTypeException {
    	Object val = getSetting(settingName);
        if (!(val instanceof Double)) {
        	throw new IncorrectSettingTypeException(settingName, "Double");
        }
        return ((Double) val).intValue();
    }

    /**
     * @param settingName Key of the setting to read.
     * @param altValue Value to return if the given setting does not exist or is of the wrong type.
     * @return Value of the setting with key settingName as a int, if it exists, else altValue.
     */
    public int getSettingAsInt(String settingName, int altValue) {
    	int ret;
    	try {
    		ret = getSettingAsInt(settingName);
    	} catch (MissingSettingException | IncorrectSettingTypeException e) {
    		ret = altValue;
    	}    	
    	return ret;
    }

    /**
     * @param settingName Key of the setting to read.
     * @return Value of the setting with key settingName as a boolean, if it exists.
     * @throws MissingSettingException If no setting with the given key exists.
     * @throws IncorrectSettingTypeException If a setting with settingName exists but is not of type boolean.
     */
    public boolean getSettingAsBoolean(String settingName) throws MissingSettingException, IncorrectSettingTypeException {
    	Object val = getSetting(settingName);
        if (!(val instanceof Boolean)) {
        	throw new IncorrectSettingTypeException(settingName, "Boolean");
        }
        return ((Boolean) val).booleanValue();
    }
    
    /**
     * @param settingName Key of the setting to read.
     * @param altValue Value to return if the given setting does not exist or is of the wrong type.
     * @return Value of the setting with key settingName as a boolean, if it exists, else altValue.
     */
    public boolean getSettingAsBoolean(String settingName, boolean altValue) {
    	boolean ret;
    	try {
    		ret = getSettingAsBoolean(settingName);
    	} catch (MissingSettingException | IncorrectSettingTypeException e) {
    		ret = altValue;
    	}    	
    	return ret;
    }

    /**
     * @param settingName Key of the setting to read.
     * @return Value of the setting with key settingName as a List, if it exists.
     * @throws MissingSettingException If no setting with the given key exists.
     * @throws IncorrectSettingTypeException If a setting with settingName exists but is not of type List.
     */
    public List<Object> getSettingAsList(String settingName) throws MissingSettingException, IncorrectSettingTypeException {
        Object val = getSetting(settingName);
        if (!(val instanceof List)) {
        	throw new IncorrectSettingTypeException(settingName, "List");
        }
        return (List<Object>) val;
    }

    /**
     * @param settingName Key of the setting to read.
     * @return Value of the setting with key settingName as a Map, if it exists.
     * @throws MissingSettingException If no setting with the given key exists.
     * @throws IncorrectSettingTypeException If a setting with settingName exists but is not of type Map.
     */
    public Map<String,Object> getSettingAsMap(String settingName) throws MissingSettingException, IncorrectSettingTypeException {
        Object val = getSetting(settingName);
        if (!(val instanceof Map)) {
        	throw new IncorrectSettingTypeException(settingName, "Map");
        }
        return (Map<String,Object>) val;
    }
    
    /**
     * @param settingName Key of the setting to assign a value to.
     * @param value Value to assign
     */
    public void setSetting(String settingName, Object value) {
    	data.put(settingName, value);
    }

    /**
     * Verifies that the loaded match settings are correct
     */
    public void checkConfiguration() throws BadMatchSetting {
    	if (getNumPlayers()<=0) {
    		throw new BadMatchSetting("Number of players must be > 0.");
    	}
    	if (getLobbyName().isEmpty()) {
    		throw new BadMatchSetting("Lobby name cannot be empty.");
    	}
    	if (getGameName().isEmpty()) {
    		throw new BadMatchSetting("Game name cannot be empty.");
    	}
    }
    
    /*
     * The following two methods are for serialization support with ObjectOutput/InputStreams
     */    
    private void writeObject(ObjectOutputStream out) throws IOException {
        Gson gson = new Gson();
        String data = gson.toJson(this.data);
        out.writeObject(data);
    }
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        Gson gson = new Gson();
        String data = (String) in.readObject();
        this.data = (Map<String, Object>) gson.fromJson(data, Object.class);
    }

}