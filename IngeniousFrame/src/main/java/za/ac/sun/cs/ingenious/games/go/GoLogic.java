package za.ac.sun.cs.ingenious.games.go;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class GoLogic implements TurnBasedGameLogic<TurnBasedSquareBoard> {
    public byte[] previousState = null;

    private Action prev1 = null;
    private Action prev2 = null;

    public GoLogic() throws IOException, MissingSettingException, IncorrectSettingTypeException {
    }

    @Override
    public boolean validMove(TurnBasedSquareBoard fromState, Move move) {
        if (move instanceof XYAction) {
            XYAction xyMove = (XYAction) move;
            byte[] originalBoard = Arrays.copyOf(fromState.board, fromState.board.length);
            byte player, opponent;
            if (move.getPlayerID() == 0) {
                player = 1;
                opponent = 2;
            } else {
                player = 2;
                opponent = 1;
            }
            int x = xyMove.getX();
            int y = xyMove.getY();
            //Check if position is open
            if (fromState.board[fromState.xyToIndex(x, y)] != 0) {
                return false;
            }
            //Check all of the possible sections around placed piece
            fromState.board[fromState.xyToIndex(x, y)] = player;
            boolean found = true;
            if (x + 1 < fromState.getBoardSize() && fromState.board[fromState.xyToIndex(x + 1, y)] == opponent) {
                byte[] newBoard = Arrays.copyOf(fromState.board, fromState.board.length);
                found = checkLiberties(fromState, newBoard, new XYAction(x + 1, y, opponent), opponent, player);
                if (!found) {
                    flipSection(fromState, new XYAction(xyMove.getX() + 1, xyMove.getY(), opponent));
                }
            }
            // If no liberties were found the pieces are flipped and the new piece is safe to play(i.e. no more flips
            // need to be checked.
            if (found && x - 1 >= 0 && fromState.board[fromState.xyToIndex(x - 1, y)] == opponent) {
                byte[] newBoard = Arrays.copyOf(fromState.board, fromState.board.length);
                found = checkLiberties(fromState, newBoard, new XYAction(x - 1, y, opponent), opponent, player);
                if (!found) {
                    flipSection(fromState, new XYAction(xyMove.getX() - 1, xyMove.getY(), opponent));
                }
            }
            if (found && y + 1 < fromState.getBoardSize() && fromState.board[fromState.xyToIndex(x, y + 1)] == opponent) {
                byte[] newBoard = Arrays.copyOf(fromState.board, fromState.board.length);
                found = checkLiberties(fromState, newBoard, new XYAction(x, y + 1, opponent), opponent, player);
                if (!found) {
                    flipSection(fromState, new XYAction(xyMove.getX(), xyMove.getY() + 1, opponent));
                }
            }
            if (found && y - 1 >= 0 && fromState.board[fromState.xyToIndex(x, y - 1)] == opponent) {
                byte[] newBoard = Arrays.copyOf(fromState.board, fromState.board.length);
                found = checkLiberties(fromState, newBoard, new XYAction(x, y - 1, opponent), opponent, player);
                if (!found) {
                    flipSection(fromState, new XYAction(xyMove.getX(), xyMove.getY() - 1, opponent));
                }
            }
            // If no tiles were flipped(i.e. liberties were found in all the opponents pieces around the new piece),
            // check if the newly placed piece has liberties.
            if (found) {
                byte[] newBoard = Arrays.copyOf(fromState.board, fromState.board.length);
                found = checkLiberties(fromState, newBoard, new XYAction(x, y, player), player, opponent);
                if (!found) {
                    // If no liberties, the move is illegal
                    fromState.board = Arrays.copyOf(originalBoard, originalBoard.length);
                    return false;
                }
            }
            //Check if board state is duplicate
            if (previousState == null) {
                fromState.board = Arrays.copyOf(originalBoard, originalBoard.length);
                return true;
            } else if (Arrays.equals(previousState, fromState.board)) {
                fromState.board = Arrays.copyOf(originalBoard, originalBoard.length);
                return false;
            } else {
                fromState.board = Arrays.copyOf(originalBoard, originalBoard.length);
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public boolean makeMove(TurnBasedSquareBoard fromState, Move move) {
        if (move instanceof XYAction) {
            XYAction xyMove = (XYAction) move;
            byte player, opponent;
            if (move.getPlayerID() == 0) {
                player = 1;
                opponent = 2;
            } else {
                player = 2;
                opponent = 1;
            }
            //Check all of the possible sections around placed piece
            fromState.board[fromState.xyToIndex(xyMove.getX(), xyMove.getY())] = (byte) (xyMove.getPlayerID() + 1);
            if (xyMove.getX() + 1 < fromState.getBoardSize() && fromState.board[fromState.xyToIndex(xyMove.getX() + 1, xyMove.getY())] == opponent) {
                byte[] newBoard = Arrays.copyOf(fromState.board, fromState.board.length);
                boolean found = checkLiberties(fromState, newBoard, new XYAction(xyMove.getX() + 1, xyMove.getY(), opponent), opponent, player);
                if (!found) {
                    flipSection(fromState, new XYAction(xyMove.getX() + 1, xyMove.getY(), opponent));
                }
            }
            if (xyMove.getX() - 1 >= 0 && fromState.board[fromState.xyToIndex(xyMove.getX() - 1, xyMove.getY())] == opponent) {
                byte[] newBoard = Arrays.copyOf(fromState.board, fromState.board.length);
                boolean found = checkLiberties(fromState, newBoard, new XYAction(xyMove.getX() - 1, xyMove.getY(), opponent), opponent, player);
                if (!found) {
                    flipSection(fromState, new XYAction(xyMove.getX() - 1, xyMove.getY(), opponent));
                }
            }
            if (xyMove.getY() + 1 < fromState.getBoardSize() && fromState.board[fromState.xyToIndex(xyMove.getX(), xyMove.getY() + 1)] == opponent) {
                byte[] newBoard = Arrays.copyOf(fromState.board, fromState.board.length);
                boolean found = checkLiberties(fromState, newBoard, new XYAction(xyMove.getX(), xyMove.getY() + 1, opponent), opponent, player);
                if (!found) {
                    flipSection(fromState, new XYAction(xyMove.getX(), xyMove.getY() + 1, opponent));
                }
            }
            if (xyMove.getY() - 1 >= 0 && fromState.board[fromState.xyToIndex(xyMove.getX(), xyMove.getY() - 1)] == opponent) {
                byte[] newBoard = Arrays.copyOf(fromState.board, fromState.board.length);
                boolean found = checkLiberties(fromState, newBoard, new XYAction(xyMove.getX(), xyMove.getY() - 1, opponent), opponent, player);
                if (!found) {
                    flipSection(fromState, new XYAction(xyMove.getX(), xyMove.getY() - 1, opponent));
                }
            }
            previousState = Arrays.copyOf(fromState.board, fromState.board.length);

            /* Record most recently played move by each player to catch the case where the enhancement causes
             idle action to be chosen by both players in which case the game infinitely loops as idle. */
            prev2 = prev1;
            prev1 = (XYAction) move;


            nextTurn(fromState, move);
        } else {

            /* Record most recently played move by each player to catch the case where the enhancement causes
             idle action to be chosen by both players in which case the game infinitely loops as idle. */
            prev2 = prev1;
            prev1 = (XYAction) move;


            nextTurn(fromState, move);
        }



        return true;
    }

    private void flipSection(TurnBasedSquareBoard fromState, XYAction start) {
        int x = start.getX();
        int y = start.getY();
        fromState.board[fromState.xyToIndex(x, y)] = 0;
        // Look around piece to see if tiles should be flipped
        if (x + 1 < fromState.getBoardSize()) {
            if (fromState.board[fromState.xyToIndex(x + 1, y)] == (byte) start.getPlayerID()) {
                flipSection(fromState, new XYAction(x + 1, y, start.getPlayerID()));
            }
        }
        if (x - 1 >= 0) {
            if (fromState.board[fromState.xyToIndex(x - 1, y)] == (byte) start.getPlayerID()) {
                flipSection(fromState, new XYAction(x - 1, y, start.getPlayerID()));
            }
        }
        if (y + 1 < fromState.getBoardSize()) {
            if (fromState.board[fromState.xyToIndex(x, y + 1)] == (byte) start.getPlayerID()) {
                flipSection(fromState, new XYAction(x, y + 1, start.getPlayerID()));
            }
        }
        if (y - 1 >= 0) {
            if (fromState.board[fromState.xyToIndex(x, y - 1)] == (byte) start.getPlayerID()) {
                flipSection(fromState, new XYAction(x, y - 1, start.getPlayerID()));
            }
        }
    }

    private boolean checkLiberties(TurnBasedSquareBoard originalBoard, byte[] copyBoard, XYAction move, int player, int opponent) {
        boolean found;
        int x = move.getX();
        int y = move.getY();
        if (x + 1 < originalBoard.getBoardSize()) {
            if (copyBoard[originalBoard.xyToIndex(x + 1, y)] == 0) {
                return true;
            } else if (copyBoard[originalBoard.xyToIndex(x + 1, y)] == player) {
                copyBoard[originalBoard.xyToIndex(x, y)] = (byte) opponent;
                found = checkLiberties(originalBoard, copyBoard, new XYAction(x + 1, y, player), player, opponent);
                if (found) {
                    return true;
                }
            }
        }
        if (x - 1 >= 0) {
            if (copyBoard[originalBoard.xyToIndex(x - 1, y)] == 0) {
                return true;
            } else if (copyBoard[originalBoard.xyToIndex(x - 1, y)] == player) {
                copyBoard[originalBoard.xyToIndex(x, y)] = (byte) opponent;
                found = checkLiberties(originalBoard, copyBoard, new XYAction(x - 1, y, player), player, opponent);
                if (found) {
                    return true;
                }
            }
        }
        if (y + 1 < originalBoard.getBoardSize()) {
            if (copyBoard[originalBoard.xyToIndex(x, y + 1)] == 0) {
                return true;
            } else if (copyBoard[originalBoard.xyToIndex(x, y + 1)] == player) {
                copyBoard[originalBoard.xyToIndex(x, y)] = (byte) opponent;
                found = checkLiberties(originalBoard, copyBoard, new XYAction(x, y + 1, player), player, opponent);
                if (found) {
                    return true;
                }
            }
        }
        if (y - 1 >= 0) {
            if (copyBoard[originalBoard.xyToIndex(x, y - 1)] == 0) {
                return true;
            } else if (copyBoard[originalBoard.xyToIndex(x, y - 1)] == player) {
                copyBoard[originalBoard.xyToIndex(x, y)] = (byte) opponent;
                found = checkLiberties(originalBoard, copyBoard, new XYAction(x, y - 1, player), player, opponent);
                if (found) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void undoMove(TurnBasedSquareBoard fromState, Move move) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Action> generateActions(TurnBasedSquareBoard fromState, int forPlayerID) {
        List<Action> actions = new ArrayList<>();
        for (int i = 0; i < fromState.getBoardSize(); i++) {
            for (int j = 0; j < fromState.getBoardSize(); j++) {
                XYAction action = new XYAction(i, j, forPlayerID);
                if (validMove(fromState, action)) {
                    actions.add(action);
                }
            }
        }

        if (prev1 != null && prev2 != null) {
            if (prev1.getPlayerID() == -1 && prev2.getPlayerID() == -1) {
                // skip idle action to avoid looping
            } else {
                // Consider skipping turn as possible move.
                if (!actions.isEmpty()) {
                    actions.add(new IdleAction());
                }
            }
        } else {
            // Consider skipping turn as possible move.
            if (!actions.isEmpty()) {
                actions.add(new IdleAction());
            }
        }

        return actions;
    }

    public List<Action> generateActions(TurnBasedSquareBoard fromState, int forPlayerID, Action previousActionByThisPlayer) {
        List<Action> actions = new ArrayList<>();
        for (int i = 0; i < fromState.getBoardSize(); i++) {
            for (int j = 0; j < fromState.getBoardSize(); j++) {
                XYAction action = new XYAction(i, j, forPlayerID);
                if (validMove(fromState, action)) {
                    if (!action.equals(previousActionByThisPlayer)) {
                        actions.add(action);
                    }
                }
            }
        }

        if (prev1 != null && prev2 != null) {
            if (prev1.getPlayerID() == -1 && prev2.getPlayerID() == -1) {
                // skip idle action to avoid looping
            } else {
                // Consider skipping turn as possible move.
                if (!actions.isEmpty()) {
                    actions.add(new IdleAction());
                }
            }
        } else {
            // Consider skipping turn as possible move.
            if (!actions.isEmpty()) {
                actions.add(new IdleAction());
            }
        }

        return actions;
    }

    @Override
    public boolean isTerminal(TurnBasedSquareBoard fromState) {
        boolean[] bothPlayable = {false, false};
        if (generateActions(fromState, 0).size() > 0) {
            bothPlayable[0] = true;
        }
        if (generateActions(fromState, 1).size() > 0) {
            bothPlayable[1] = true;
        }
        if (bothPlayable[0] && bothPlayable[1]) {
            return false;
        }
        return true;
    }
}
