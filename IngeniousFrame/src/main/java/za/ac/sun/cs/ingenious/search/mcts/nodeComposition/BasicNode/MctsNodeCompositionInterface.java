package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode;

import za.ac.sun.cs.ingenious.core.util.search.TreeNode;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Action;
import java.util.List;

public interface MctsNodeCompositionInterface<S extends GameState, C, P> extends TreeNode<S, C, P> {
	
	double getVisitCount();
	
	void incVisitCount();
	
	void decVisitCount();
	
	void incChildVisitCount(C child);
	
	double getChildVisitCount(C child);
	
	double getChildValue(C child);
	
	List<Action> getUnexploredChildren();
	
	boolean isUnexploredEmpty();
	
	C getAnUnexploredChild(GameLogic<S> logic);

	C getAnUnexploredChild(GameLogic<S> logic, int index);
	
	Action getPrevAction();
	
	C getSelfAsChildType();

	int getPlayerID();
	
}
