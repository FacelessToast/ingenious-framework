package za.ac.sun.cs.ingenious.search.mcts.selection;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

import java.util.List;

/**
 * A standard implementation of the TreeEngine selection strategy,
 * that calculates UCT values to make a decision of which child node to select.
 * This strategy is specifically for TreeEngine selection and makes use of read locks
 * when viewing node information that is used in the calculations.
 *
 * @author Karen Laubscher
 *
 * @param <S>  The game state type.
 * @param <N>  The type of the mcts node.
 */
public class TreeSelectionUct<S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, ?>> implements TreeSelection<N> {
	
	private GameLogic<S> logic;
	private static double c;
	public int player;

	/**
	 * Constructor to create a TreeEngine UCT selection object with a specified value
	 * for the constant C used in the UCT value calculation.
	 *
	 * @param logic  The game logic
	 * @param c      The constant value for C (in the UCT calculation).
	 */
	private TreeSelectionUct(GameLogic<S> logic, double c, int player) {
		this.logic = logic;
		this.c = c;
		this.player = player;
	}
	
	
	/**
	 * The method that decide which child node to traverse to next, based on 
	 * calculating the UCT value for each child and then selecting the child 
	 * with the highest UCT value. Since the TreeEngine sturture is shared in TreeEngine
	 * parallelisation, nodes are (read) locked when information for
	 * the calculations are viewed.
	 * 
	 * @param node  The current node whose children nodes are considered for the
	 *				next node to traverse to.
	 * 
	 * @return      The selected child node with the highest UCT value.
	 */
	public N select(N node) {
        //Terminal state - no further selection takes place, therefore return node as selected node

		if (logic.isTerminal(node.getState())) {
              return node;
		}

        List<N> children;
		double currentVisitCount;
		children = node.getChildren();

		currentVisitCount = node.getVisitCount();
		N highestUctChild = null;
		double tempVal;
		double highestUct = Double.NEGATIVE_INFINITY;

		for (N child : children) {
//			if (player == node.getPlayerID()) {
//				tempVal = child.getValue()/child.getVisitCount() + c*Math.sqrt((2*Math.log(currentVisitCount)/child.getVisitCount()));
//			} else {
				tempVal = (1-child.getValue()/child.getVisitCount()) + c*Math.sqrt((2*Math.log(currentVisitCount)/child.getVisitCount()));
//			}

			if (Double.compare(tempVal, highestUct) > 0) {
				highestUctChild = child;
				highestUct = tempVal;
			}
		}

		if (highestUctChild == null) {
			return null;
		}

		// update virtual loss to aid in parallel efficiency
		highestUctChild.applyVirtualLoss(-10);
        return highestUctChild;
	}

	
	/**
	 * A static factory method, which creates a TreeEngine UCT selection object that
	 * uses the specified value for the constant C in the UCT value calculation.
	 * 
	 * This factory method is also a generic method, which uses Java type 
	 * inferencing to encapsulate the complex generic type capturing required 
	 * by the TreeEngine UCT selection constructor.
	 *
	 * @param logic   The game logic
	 * @param c       The constant value for C (in the UCT calculation).
	 */
	public static <SS extends GameState, NN extends MctsNodeTreeParallelInterface<SS, NN, ?>> TreeSelection<NN> newTreeSelectionUct(GameLogic<SS> logic, double c, int player) {
		return new TreeSelectionUct<>(logic, c, player);
	}

}
