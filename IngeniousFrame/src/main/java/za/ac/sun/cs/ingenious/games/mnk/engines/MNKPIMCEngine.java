package za.ac.sun.cs.ingenious.games.mnk.engines;

import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.games.mnk.MNKEngine;
import za.ac.sun.cs.ingenious.games.mnk.MNKFinalEvaluator;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSDescender;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSNode;
import za.ac.sun.cs.ingenious.search.mcts.legacy.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.legacy.SimpleUpdater;
import za.ac.sun.cs.ingenious.search.mcts.legacy.UCT;
import za.ac.sun.cs.ingenious.search.pimc.PIMC;

public class MNKPIMCEngine extends MNKEngine {

	public MNKPIMCEngine(EngineToServerConnection toServer) {
		super(toServer);
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	@Override
	public String engineName() {
		return "MNKPIMCEngine";
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		return new PlayActionMessage(PIMC.generateAction(board, logic, logic,
				new RandomPolicy<MNKState>(logic, new MNKFinalEvaluator(), logic, false),
				new MCTSDescender<MNKState>(logic, new UCT<>(), new PerfectInformationActionSensor<>()), 
				new SimpleUpdater<MCTSNode<MNKState>>(), 
				logic, playerID, Constants.TURN_LENGTH, 80));
	}

}
