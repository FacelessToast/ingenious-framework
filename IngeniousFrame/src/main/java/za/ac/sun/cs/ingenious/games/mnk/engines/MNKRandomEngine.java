package za.ac.sun.cs.ingenious.games.mnk.engines;

import java.util.List;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.mnk.MNKEngine;

public class MNKRandomEngine extends MNKEngine {

	public MNKRandomEngine(EngineToServerConnection toServer) {
		super(toServer);
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	@Override
	public String engineName() {
		return "MNKRandomEngine";
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		List<Action> al = logic.generateActions(board, playerID);
		if (al.isEmpty()) {
			return new PlayActionMessage(null);
		}
		Random rand = new Random();
		int r = rand.nextInt(al.size());
		return new PlayActionMessage(al.get(r));
	}

}
