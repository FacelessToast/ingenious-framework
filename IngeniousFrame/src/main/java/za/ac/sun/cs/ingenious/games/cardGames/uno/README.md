#Uno

####This is a simplified version of Uno (rules below). See classical game rules [*here*](http://www.unorules.com)

**Simplified rules**

* Turn based card game.
* **Number of players:** max eight players allowed.
* **Goal:** be the first one to get rid of all your hand cards.
* There're **no special cards** (i. e. only cards from 0 - 9 with colors red, blue, green and yellow exist).
* Every player initially gets **7 cards**.
* If it's your turn you have the option to either put a card from your hand on top of the discard pile 
(if the card matches the current top) or to draw a card from the draw pile.
* **Matching:** like in Uno's standard rules: you're only allowed to put a card on the discard pile if your color or your suit matches the current top.
* **Important:** in this version you are **not** allowed to play a card after you have already drawn from the draw pile. 
* **Game ends** when the first player has no more cards on his hand.
