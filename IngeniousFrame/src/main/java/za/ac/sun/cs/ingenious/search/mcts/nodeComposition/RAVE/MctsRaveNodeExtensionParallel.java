package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MctsRaveNodeExtensionParallel<S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> implements MctsNodeExtensionParallelInterface {

    // lock for RaveVisitCount
    private final ReadWriteLock lockRaveVisitCount = new ReentrantReadWriteLock();
    private final Lock writeLockRaveVisitCount = lockRaveVisitCount.writeLock();
    private final Lock readLockRaveVisitCount = lockRaveVisitCount.readLock();

    // lock for RaveWinCount
    private final ReadWriteLock lockRaveWinCount = new ReentrantReadWriteLock();
    private final Lock writeLockRaveWinCount = lockRaveWinCount.writeLock();
    private final Lock readLockRaveWinCount = lockRaveWinCount.readLock();

    // lock for RAVE
    private final ReadWriteLock lockRAVE = new ReentrantReadWriteLock();
    private final Lock writeLockRAVE = lockRAVE.writeLock();
    private final Lock readLockRAVE = lockRAVE.readLock();

    // lock for ActionToNodeMapping
    private final ReadWriteLock lockActionToNodeMapping = new ReentrantReadWriteLock();
    private final Lock writeLockActionToNodeMapping = lockActionToNodeMapping.writeLock();
    private final Lock readLockActionToNodeMapping = lockActionToNodeMapping.readLock();

    // lock for setUp
    private final ReadWriteLock locksetUp = new ReentrantReadWriteLock();
    private final Lock writeLocksetUp = locksetUp.writeLock();
    private final Lock readLocksetUp = locksetUp.readLock();

    // lock for ID
    private final ReadWriteLock lockID = new ReentrantReadWriteLock();
    private final Lock writeLockID = lockID.writeLock();
    private final Lock readLockID = lockID.readLock();

    // lock for child
    private final ReadWriteLock lockChild = new ReentrantReadWriteLock();
    private final Lock writeLockChild = lockChild.writeLock();
    private final Lock readLockChild = lockChild.readLock();


    private Hashtable<Action, Integer> virtualLosses = new Hashtable<>();

    MctsRaveNodeExtensionComposition raveNodeExtensionBasic;

    /**
     * Constructor which initialises the extension composition object for which this class is a wrapper
     */
    public MctsRaveNodeExtensionParallel() {
        raveNodeExtensionBasic = new MctsRaveNodeExtensionComposition();
    }

    /**
     * perform the set up of respective fields relating to the type of node for which this class is a wrapper
     * @param node
     */
    public void setUp(MctsNodeComposition node) {
        writeLocksetUp.lock();
        try {
            raveNodeExtensionBasic.setUp(node);
        } finally {
            writeLocksetUp.unlock();
        }
    }

    /**
     * @return the id of the next player to play for the node object for which this extension relates
     */
    public String getID() {
        readLockID.lock();
        try {
            return raveNodeExtensionBasic.getID();
        } finally {
            readLockID.unlock();
        }
    }

    /**Prints to log the win to visit rate of all children as well as the value attributed to that move according
     * to the relevant implementation (This value would be used during selection to choose an action) */
    public <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> void logPossibleMoves(N child) {
         Log.debug("MCTS", "\t\t Rave winrate: " + (int)this.getRaveWins(child.getPrevAction()) + "/" + this.getRaveVisits(child.getPrevAction()) + "(" + Math.round(100*this.getRaveWins(child.getPrevAction())/this.getRaveVisits(child.getPrevAction())*100.0)/100.0
                + "%)");
    }

    /**
     * set the read lock for this node extension
     */
    public void setReadLockChild() {
        readLockChild.lock();
    }

    /**
     * unset the read lock for this node extension
     */
    public void unsetReadLockChild() {
        readLockChild.unlock();
    }

    /**
     * @param action
     * @return the visit count rave value for the child relating to the action given as parameter
     */
    public int getRaveVisits(Action action) {
        readLockRaveVisitCount.lock();
        try {
            return raveNodeExtensionBasic.getRaveVisits(action);
        } finally {
            readLockRaveVisitCount.unlock();
        }
    }

    /**
     * @param action
     * @return the win count rave value for the child relating to the action given as parameter
     */
    public int getRaveWins(Action action) {
        readLockRaveWinCount.lock();
        try {
            return raveNodeExtensionBasic.getRaveWins(action);
        } finally {
            readLockRaveWinCount.unlock();
        }
    }

    /**
     * updates the rave win and visit count fields for children of the current node's
     * @param win
     * @param movesOnPath
     */
    public void updateRaveValues(boolean win, ArrayList<Action> movesOnPath) {
        writeLockRAVE.lock();
        try {
            raveNodeExtensionBasic.updateRaveValues(win, movesOnPath);
        } finally {
            writeLockRAVE.unlock();
        }
    }

    /**
     * @return action to node mapping for use in the selection phase of the rave implementation
     */
    public Hashtable<Action, MctsNodeTreeParallelInterface<?, ?, ?>> getActionToNodeMapping() {
        readLockActionToNodeMapping.lock();
        try {
            return raveNodeExtensionBasic.getActionToNodeMapping();
        } finally {
            readLockActionToNodeMapping.unlock();
        }
    }

    /**
     * Update the action to node mapping for use in the selection phase of the rave implementation
     * @param child
     */
    public <S extends GameState> void addChild(MctsNodeTreeParallel<S> child) {
        writeLockChild.lock();
        try {
            raveNodeExtensionBasic.addChild(child);
        } finally {
            writeLockChild.unlock();
        }
    }

    /**
     * Update the action to node mapping for use in the selection phase of the rave implementation
     * @param newChildren
     */
    public <S extends GameState> void addChildren(List<MctsNodeTreeParallel<S>> newChildren) {
        writeLockChild.lock();
        try {
            raveNodeExtensionBasic.addChildren(newChildren);
        } finally {
            writeLockChild.unlock();
        }
    }
}
