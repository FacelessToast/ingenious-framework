package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.PW;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MctsPWNodeExtensionParallel<S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> implements MctsNodeExtensionParallelInterface {

    // lock for Children
    private final ReadWriteLock lockChildren = new ReentrantReadWriteLock();
    private final Lock writeLockChildren = lockChildren.writeLock();
    private final Lock readLockChildren = lockChildren.readLock();

    // lock for pruned children
    private final ReadWriteLock lockPrunedChildren = new ReentrantReadWriteLock();
    private final Lock writeLockPrunedChildren = lockPrunedChildren.writeLock();
    private final Lock readLockPrunedChildren = lockPrunedChildren.readLock();

    // lock for setUp
    private final ReadWriteLock locksetUp = new ReentrantReadWriteLock();
    private final Lock writeLocksetUp = locksetUp.writeLock();

    // lock for ID
    private final ReadWriteLock lockID = new ReentrantReadWriteLock();
    private final Lock readLockID = lockID.readLock();

    // lock for child
    private final ReadWriteLock lockChild = new ReentrantReadWriteLock();
    private final Lock readLockChild = lockChild.readLock();


    MctsPWNodeExtensionComposition PWNodeExtensionBasic;

    /**
     * constructor to instantiate the node extension composition object for which this class is a wrapper
     */
    public MctsPWNodeExtensionParallel() {
        PWNodeExtensionBasic = new MctsPWNodeExtensionComposition();
    }

    /**
     * perform relevant set up protocol for the extension composition object
     * @param node
     */
    public void setUp(MctsNodeComposition node) {
        writeLocksetUp.lock();
        try {
            PWNodeExtensionBasic.setUp(node);
        } finally {
            writeLocksetUp.unlock();
        }
    }

    /**
     * @return the id relating to the player to play next for the node for which this extension object applies
     */
    public String getID() {
        readLockID.lock();
        try {
            return PWNodeExtensionBasic.getID();
        } finally {
            readLockID.unlock();
        }
    }

    /**Prints to log the win to visit rate of all children as well as the value attributed to that move according
     * to the relevant implementation (This value would be used during selection to choose an action) */
    public <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> void logPossibleMoves(N child) {
         Log.debug("MCTS", "\t\t Progressive Widening winrate: " + (int)child.getValue() + "/" + child.getVisitCount() + "(" + Math.round(100*child.getValue()/child.getVisitCount()*100.0)/100.0
                + "%)");
    }

    /**
     * Add a child to the list of pruned children reachable from the node for which this extension applies
     * @param child
     * @param <S>
     */
    public <S extends GameState> void addChild(MctsNodeTreeParallel<S> child){
        writeLockPrunedChildren.lock();
        try {
            PWNodeExtensionBasic.addChildToPrunedChildren(child);
        } finally {
            writeLockPrunedChildren.unlock();
        }
    }

    /**
     * Adds children to the list of pruned children reachable from the node for which this extension applies
     * @param newChildren the list of new children to add
     * @param <S>
     */
    public <S extends GameState> void addChildren(List<MctsNodeTreeParallel<S>> newChildren){}

    /**
     * locks the list of pruned children with a writelock
     */
    public void setWriteLockPrunedChildred() {
        writeLockPrunedChildren.lock();
    }

    /**
     * unlocks the list of pruned children with a writelock
     */
    public void unsetWriteLockPrunedChildred() {
        writeLockPrunedChildren.unlock();
    }

    /**
     * @return the list of unpruned and explored children
     */
    public ArrayList<N> getUnprunedChildren() {
        readLockChildren.lock();
        try {
            return PWNodeExtensionBasic.getUnprunedChildren();
        } finally {
            readLockChildren.unlock();
        }
    }

    /**
     * @return the size of the list of unpruned and explored children
     */
    public int getUnprunedChildrenSize() {
        readLockChildren.lock();
        try {
            return PWNodeExtensionBasic.getUnprunedChildrenSize();
        } finally {
            readLockChildren.unlock();
        }
    }

    /**
     * adds a child to the list of unpruned and explored children
     * @param child
     */
    public <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> void addChildToUnprunedChildren(N child) {
        writeLockChildren.lock();
        try {
            PWNodeExtensionBasic.addChildToUnprunedChildren(child);
        } finally {
            writeLockChildren.unlock();
        }
    }

    /**
     * @return the list of pruned and unexplored children
     */
    public ArrayList<N> getPrunedChildren() {
        readLockPrunedChildren.lock();
        try {
            return PWNodeExtensionBasic.getPrunedChildren();
        } finally {
            readLockPrunedChildren.unlock();
        }
    }

    /**
     * @return the list of pruned and unexplored children without locking the table while reading from it
     */
    public ArrayList<N> getPrunedChildrenNoLock() {
        return PWNodeExtensionBasic.getPrunedChildren();
    }

    /**
     * @return the size of the list of pruned and unexplored children
     */
    public int getPrunedChildrenSize() {
        readLockPrunedChildren.lock();
        try {
            return PWNodeExtensionBasic.getPrunedChildrenSize();
        } finally {
            readLockPrunedChildren.unlock();
        }
    }

    /**
     * Adds a number of children to the list of pruned and unexplored children
     * @param child
     */
    public <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> void addChildToPrunedChildren(N child) {
        writeLockPrunedChildren.lock();
        try {
            PWNodeExtensionBasic.addChildToPrunedChildren(child);
        } finally {
            writeLockPrunedChildren.unlock();
        }
    }

    /**
     * removes a child at a specified index in the pruned and unexplored children array
     * @param index
     */
    public void removeChildFromPrunedChildren(int index) {
        writeLockPrunedChildren.lock();
        try {
            PWNodeExtensionBasic.removeChildFromPrunedChildren(index);
        } finally {
            writeLockPrunedChildren.unlock();
        }
    }

    /**
     * add the list of all children to the list of children, this is then used to remove children as they are
     * unpruned and explored
     * @param children
     */
    public void instantiatePrunedChildren(List<MctsNodeComposition> children) {
        writeLockPrunedChildren.lock();
        try {
            PWNodeExtensionBasic.instantiatePrunedChildren(children);
        } finally {
            writeLockPrunedChildren.unlock();
        }
    }

    /**
     * returns the child at the specified index in the pruned children array list
     * @param index
     * @return
     */
    public N getPrunedChild(int index) {
        readLockPrunedChildren.lock();
        try {
            return (N)PWNodeExtensionBasic.getPrunedChild(index);
        } finally {
            readLockPrunedChildren.unlock();
        }
    }

    /**
     * returns the child at the specified index in the pruned children array list without
     * using a read lock when doing this
     * @param index
     * @return
     */
    public N getPrunedChildNoLocks(int index) {
        return (N)PWNodeExtensionBasic.getPrunedChild(index);
    }

    /**
     * removes a child from the pruned children list without using locks when performing this operation
     * @param index
     */
    public void removeChildFromPrunedChildrenNoLocks(int index) {
        PWNodeExtensionBasic.removeChildFromPrunedChildren(index);
    }

}
