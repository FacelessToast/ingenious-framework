package za.ac.sun.cs.ingenious.games.go.engines.TreeEngine;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.ForfeitAction;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.games.go.GoEngine;
import za.ac.sun.cs.ingenious.games.go.GoMctsFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.MctsProcess.MctsTree;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.*;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.CMCTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.MctsCMCNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.LGR.LGRTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST.MastTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.PW.MctsPWNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE.MctsRaveNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelection;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionFinal;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;

import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage.newBackpropagationAverage;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationContextual.newBackpropagationContextual;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationLgr.newBackpropagationLgr;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationLgrf.newBackpropagationLgrf;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationMast.newBackpropagationMast;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationRave.newBackpropagationRave;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationSimple.newBackpropagationSimple;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionMast.newExpansionMast;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle.newExpansionSingle;
import static za.ac.sun.cs.ingenious.search.mcts.selection.FinalSelectionUct.newFinalSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionPW.newTreeSelectionProgressiveWidening;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionRave.newTreeSelectionRave;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUct.newTreeSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUctFPU.newTreeSelectionFPU;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUctTunedFPU.newTreeSelectionFPUTuned;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationContextual.newSimulationContextual;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrfFull.newSimulationLgrFull;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrTree.newSimulationLgrTree;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrFull.newSimulationLgrfFull;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationMastFullTree.newSimulationMastFullTree;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationMastTreeOnly.newSimulationMastTreeOnly;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom.newSimulationRandom;

public class GoMCTSTreeGenericEngine extends GoEngine {

    MctsTree<TurnBasedSquareBoard, MctsNodeTreeParallel<TurnBasedSquareBoard>> mcts;
    ArrayList<BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>>> backpropagationEnhancements = new ArrayList<>();
    Hashtable<String, MctsNodeExtensionParallelInterface> enhancementExtensionClasses = new Hashtable<>();
    String playerEnhancementConfig;

    protected int TURN_LENGTH = 3000;
    protected int THREAD_COUNT;

    private MastTable visitedMovesTableMast = null;
    private LGRTable visitedMovesTableLgr = null;
    private CMCTable visitedMovesTableCmc = null;

    MctsNodeTreeParallel<TurnBasedSquareBoard> root;

    /**
     * @param toServer An established connection to the GameServer
     */
    public GoMCTSTreeGenericEngine(EngineToServerConnection toServer, String enhancementConfig, int threadCount) throws IOException, MissingSettingException, IncorrectSettingTypeException {
        super(toServer);

        THREAD_COUNT = threadCount;

        int length = enhancementConfig.length();
        playerEnhancementConfig = enhancementConfig.substring(36,length-5);

        MatchSetting m = new MatchSetting(enhancementConfig);

        String SelectionEnhancement = m.getSettingAsString("Selection");
        String ExpansionEnhancement = m.getSettingAsString("Expansion");
        String SimulationEnhancement = m.getSettingAsString("Simulation");
        String BackpropagationEnhancements = m.getSettingAsString("Backpropagation");
        String[] BackpropagationEnhancement = BackpropagationEnhancements.split(",");

        // Selection strategy object
        TreeSelection<MctsNodeTreeParallel<TurnBasedSquareBoard>> selection = getSelectionClass(SelectionEnhancement);

        // Final Selection strategy object
        TreeSelectionFinal<MctsNodeTreeParallel<TurnBasedSquareBoard>> finalSelection = newFinalSelectionUct(logic);

        // Simulation strategy object
        boolean recordMoves = false;
        if (SelectionEnhancement.equals("Rave")) {
            recordMoves = true;
        }
        SimulationThreadSafe<TurnBasedSquareBoard> simulation = getSimulationClass(SimulationEnhancement, recordMoves);

        // Expansion strategy object
        ExpansionThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>,MctsNodeTreeParallel<TurnBasedSquareBoard>> expansion = getExpansionClass(ExpansionEnhancement);


        // Backpropagation strategy objects
        int size = BackpropagationEnhancement.length;
        for (int i = 0; i < size; i++) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>> backpropagationEnhancement = getBackpropagationClass(BackpropagationEnhancement[i]);
            if (backpropagationEnhancement != null) {
                backpropagationEnhancements.add(backpropagationEnhancement);
            }
        }

        this.mcts = new MctsTree<>(selection, expansion, simulation, backpropagationEnhancements, finalSelection, logic, THREAD_COUNT, playerID);

//        MatchSetting boardSettings = new MatchSetting("Go.json");
//        int boardSize = boardSettings.getSettingAsInt("boardSize");
//        zobrist = new ZobristHashing(boardSize);
//        GoLogic goLogic = logic;
//        zobrist = goLogic.getZobrist();
    }

    @Override
    public String engineName() {
        return "GoMCTSTreeVanillaEngine";
    }

    @Override
    /**
     * Terminal state has been reached, end game.
     */
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        GoMctsFinalEvaluator eval = new GoMctsFinalEvaluator();
        Log.info("Game has terminated");
        Log.info("Final scores:");
        double[] score = eval.getScore(currentState);
        for (int i = 0; i < score.length; i++) {
            try(FileWriter fw = new FileWriter("ResultsGo.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw))
            {
                if (i == playerID) {
                    String str = playerEnhancementConfig + ": " + score[i];
                    out.println(str);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.info("Final state:");
        currentState.printPretty();
        toServer.closeConnection();
        System.exit(0);
    }

    @Override
    /**
     * Use MCTS to compute the best action to play on this player's next move in the game
     */
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Hashtable<String, MctsNodeExtensionParallelInterface> newEnhancementExtensionClasses = new Hashtable<>();
        if(visitedMovesTableMast != null) {
            visitedMovesTableMast.resetVisitedMoves();
        }

        if(visitedMovesTableLgr != null) {
            visitedMovesTableLgr.resetVisitedMoves();
        }

        if(visitedMovesTableCmc != null) {
            visitedMovesTableCmc.resetVisitedMoves();
        }
        for (MctsNodeExtensionParallelInterface newExtension: enhancementExtensionClasses.values()) {
            try {
                newEnhancementExtensionClasses.put(newExtension.getID(), newExtension.getClass().newInstance());
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

//        if (currentStateNode != null && currentStateNode.getPlayerID() == playerID) {
//            root = currentStateNode;
//        } else {
//            root = new MctsNodeTreeParallel<>(currentState, null, null, new ArrayList<>(), logic, newEnhancementExtensionClasses, playerID, previousRootAction);
//        }

        root = new MctsNodeTreeParallel<>(currentState, null, null, new ArrayList<>(), logic, newEnhancementExtensionClasses, playerID);

        MctsNodeTreeParallel finalChoice = mcts.doSearch(root, TURN_LENGTH, zobrist);
        Action action = finalChoice.getPrevAction();

        if (action == null) {
            return new PlayActionMessage(new ForfeitAction((byte) playerID));
        }

        return new PlayActionMessage(action);
    }

    /**
     * Create object for the selection class specified in the .json file.
     * @param selectionClass
     * @return
     */
    public TreeSelection<MctsNodeTreeParallel<TurnBasedSquareBoard>> getSelectionClass(String selectionClass) {
        if (selectionClass.equals("Uct")) {
            return newTreeSelectionUct(logic, 0.24, playerID);
        } else if (selectionClass.equals("UctFPU")) {
            return newTreeSelectionFPU(logic, 1.89248, 0.24, playerID);
        } else if (selectionClass.equals("UctTunedFPU")) {
            return newTreeSelectionFPUTuned(logic, 1.89248, 0.24, playerID);
        } else if (selectionClass.equals("Rave")) {
            return newTreeSelectionRave(logic, 0.24, 1000, playerID);
        } else if (selectionClass.equals("PW")) {
            MctsPWNodeExtensionParallel parallelPWNodeExtension = new MctsPWNodeExtensionParallel();
            enhancementExtensionClasses.put("PW", parallelPWNodeExtension);
            return newTreeSelectionProgressiveWidening(logic);
        } else {
            System.out.println("\nInvalid selection strategy defined in the EnhancementChoice.json file.\n");
            System.exit(0);
        }
        return null;
    }

    /**
     * Create object for the expansion class specified in the .json file.
     * @param expansionClass
     * @return
     */
    public ExpansionThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>,MctsNodeTreeParallel<TurnBasedSquareBoard>> getExpansionClass(String expansionClass) {
        if (expansionClass.equals("Single")) {
            return newExpansionSingle(logic);
        } else if (expansionClass.equals("Mast")) {
            return newExpansionMast(logic, visitedMovesTableMast, 30.0);
        } else {
            System.out.println("\nInvalid expansion strategy defined in the EnhancementChoice.json file.\n");
            System.exit(0);
        }
        return null;
    }

    /**
     * Create object for the simulation class specified in the .json file.
     * @param simulationClass
     * @param recordMoves
     * @return
     */
    public SimulationThreadSafe<TurnBasedSquareBoard> getSimulationClass(String simulationClass, boolean recordMoves) {
        if (simulationClass.equals("Random")) {
            return newSimulationRandom(logic, new GoMctsFinalEvaluator(), new PerfectInformationActionSensor<TurnBasedSquareBoard>(), recordMoves);
        } else if (simulationClass.equals("MastTreeOnly")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>> backpropagationMast = newBackpropagationMast();
            backpropagationEnhancements.add(backpropagationMast);
            visitedMovesTableMast =((BackpropagationMast<MctsNodeTreeParallel<TurnBasedSquareBoard>>) backpropagationMast).getQMastTable(1.0);
            return newSimulationMastTreeOnly(logic, new GoMctsFinalEvaluator(), new PerfectInformationActionSensor<TurnBasedSquareBoard>(), visitedMovesTableMast, 1.0, recordMoves);
        } else if (simulationClass.equals("MastFullTree")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>> backpropagationMast = newBackpropagationMast();
            backpropagationEnhancements.add(backpropagationMast);
            visitedMovesTableMast =((BackpropagationMast<MctsNodeTreeParallel<TurnBasedSquareBoard>>) backpropagationMast).getQMastTable(1.0);
            return newSimulationMastFullTree(logic, new GoMctsFinalEvaluator(), new PerfectInformationActionSensor<TurnBasedSquareBoard>(), visitedMovesTableMast, 1.0, recordMoves);
        } else if (simulationClass.equals("Contextual")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>> backpropagationContextual = newBackpropagationContextual();
            backpropagationEnhancements.add(backpropagationContextual);
            visitedMovesTableCmc =((BackpropagationContextual<MctsNodeTreeParallel<TurnBasedSquareBoard>>) backpropagationContextual).getCMCTable();
            return newSimulationContextual(logic, new GoMctsFinalEvaluator(), new PerfectInformationActionSensor<TurnBasedSquareBoard>(), visitedMovesTableCmc, 0.67, 0.21, recordMoves);
        } else if (simulationClass.equals("LgrTree")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>> backpropagation = newBackpropagationLgr();
            backpropagationEnhancements.add(backpropagation);
            visitedMovesTableLgr =((BackpropagationLgr<MctsNodeTreeParallel<TurnBasedSquareBoard>>) backpropagation).getLGRTable();
            return newSimulationLgrTree(logic, new GoMctsFinalEvaluator(), new PerfectInformationActionSensor<TurnBasedSquareBoard>(), visitedMovesTableLgr, recordMoves);
        } else if (simulationClass.equals("LgrFull")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>> backpropagation = newBackpropagationLgr();
            backpropagationEnhancements.add(backpropagation);
            visitedMovesTableLgr =((BackpropagationLgr<MctsNodeTreeParallel<TurnBasedSquareBoard>>) backpropagation).getLGRTable();
            return newSimulationLgrFull(logic, new GoMctsFinalEvaluator(), new PerfectInformationActionSensor<TurnBasedSquareBoard>(), visitedMovesTableLgr, recordMoves);
        } else if (simulationClass.equals("LgrfTree")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>> backpropagation = newBackpropagationLgrf();
            backpropagationEnhancements.add(backpropagation);
            visitedMovesTableLgr =((BackpropagationLgrf<MctsNodeTreeParallel<TurnBasedSquareBoard>>) backpropagation).getLGRTable();
            return newSimulationLgrTree(logic, new GoMctsFinalEvaluator(), new PerfectInformationActionSensor<TurnBasedSquareBoard>(), visitedMovesTableLgr, recordMoves);
        } else if (simulationClass.equals("LgrfFull")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>> backpropagation = newBackpropagationLgrf();
            backpropagationEnhancements.add(backpropagation);
            visitedMovesTableLgr =((BackpropagationLgrf<MctsNodeTreeParallel<TurnBasedSquareBoard>>) backpropagation).getLGRTable();
            return newSimulationLgrfFull(logic, new GoMctsFinalEvaluator(), new PerfectInformationActionSensor<TurnBasedSquareBoard>(), visitedMovesTableLgr, recordMoves);
        } else {
            System.out.println("\nInvalid simulation strategy defined in the EnhancementChoice.json file.\n");
            System.exit(0);
        }
        return null;
    }

    /**
     * Create object for the backpropagation class specified in the .json file.
     * @param backpropagationClass
     * @return
     */
    public BackpropagationThreadSafe<MctsNodeTreeParallel<TurnBasedSquareBoard>> getBackpropagationClass(String backpropagationClass) {
        if (backpropagationClass.equals("Average")) {
            return newBackpropagationAverage();
        } else if (backpropagationClass.equals("Simple")) {
            return newBackpropagationSimple(playerID);
        } else if (backpropagationClass.equals("Mast")) {
            return null;
        } else if (backpropagationClass.equals("Rave")) {
            MctsRaveNodeExtensionParallel parallelRaveNodeExtension = new MctsRaveNodeExtensionParallel();
            enhancementExtensionClasses.put("Rave", parallelRaveNodeExtension);
            return newBackpropagationRave();
        } else if (backpropagationClass.equals("Contextual")) {
            MctsCMCNodeExtensionParallel parallelCMCNodeExtension = new MctsCMCNodeExtensionParallel();
            enhancementExtensionClasses.put("Contextual", parallelCMCNodeExtension);
            return null;
        } else {
            System.out.println("\nInvalid backpropagation strategy defined in the EnhancementChoice.json file.\n");
            System.exit(0);
        }
        return null;
    }
}
