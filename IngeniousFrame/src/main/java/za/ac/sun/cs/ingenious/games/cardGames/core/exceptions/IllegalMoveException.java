package za.ac.sun.cs.ingenious.games.cardGames.core.exceptions;

/**
 * The IllegalMoveException.
 * 
 * Will be raised if a received move type was not expected.
 * 
 * @author Joshua Wiebe
 */
public class IllegalMoveException extends Exception{

	/** The serialVersionUID. */
	private static final long serialVersionUID = 34873829437L;
	
	/**
	 * Instantiates a new IllegalMoveException.
	 *
	 * @param Message for super class.
	 */
	public IllegalMoveException(String message){
		super(message);
	}
	
}
