package za.ac.sun.cs.ingenious.core.network.lobby.messages;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.Message;

/**
 * Message to indicate that the client wants to open a new lobby.
 * Contains the match settings for the game to create a lobby for.
 * @author Stephan Tietz
 *
 */
public class NewMatchMessage extends Message {

	private static final long serialVersionUID = 1L;
	private MatchSetting ms;

	public NewMatchMessage(MatchSetting ms) {
		this.ms = ms;
	}
	
	public MatchSetting getMatchSetting(){
		return ms;
	}
}
