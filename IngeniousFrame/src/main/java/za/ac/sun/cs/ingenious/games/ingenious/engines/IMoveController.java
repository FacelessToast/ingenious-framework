package za.ac.sun.cs.ingenious.games.ingenious.engines;

import za.ac.sun.cs.ingenious.games.ingenious.IngeniousEngine;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;

public interface IMoveController {
	public IngeniousAction generateMove(IngeniousEngine engine,IngeniousEvaluator evaluator);
	
}
