package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.DrawCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.PlayCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;

/**
 * The class UnoGameLogic.
 * 
 * Provides a bunch of methods to obtain further information about a game state or modify it. 
 * 
 * @author Joshua Wiebe
 */
public class UnoGameLogic implements TurnBasedGameLogic<UnoGameState> {

	/**
	 * @see za.ac.sun.cs.ingenious.core.GameLogic#generateMoves(int, za.ac.sun.cs.ingenious.core.model.GameState)
	 * 
	 * Generates all possible  moves for a given state
	 * 
	 * @param forPlayerID The player for which moves are generated.
	 * @param fromState Current state.
	 * 
	 * @return Returns an ArrayList containing all possible moves.
	 */
	@Override
	public List<Action> generateActions(UnoGameState fromState, int forPlayerID) {
		
		// ArrayList for all possible moves generated.
		ArrayList<PlayCardAction<UnoLocations>> moves = new ArrayList<>();
		
		// Fetch players location in the location enum.
		UnoLocations playerLocation = UnoLocations.values()[forPlayerID];
		
		// Fetch card-location-map which.
		TreeMap<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> map = fromState.getMap();

		// Get current Top
		Card<UnoSymbols, UnoSuits> currentTop = fromState.getTop();
		
		// For each card type
		for (Map.Entry<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> entry : map.entrySet()) {
			Card<UnoSymbols, UnoSuits> card = entry.getKey();
			
			// If top is not empty and also card type matches top (either suit OR symbol).
			if (currentTop != null && (card.getf1().equals(currentTop.getf1())
					|| card.getf2().equals(currentTop.getf2()))) {
				
				// Check if player has this card.
				for (UnoLocations location : entry.getValue()) {
					
					// If so: create a move and add to list.
					if (location.equals(playerLocation)) {
						moves.add(new PlayCardAction<UnoLocations>(forPlayerID, card, playerLocation,
								UnoLocations.DISCARDPILE));
					}
				}
				
			// If top is empty: every card the player is holding is a possible move.
			} else if (currentTop == null) {
				for (UnoLocations location : entry.getValue()) {
					if (location.equals(playerLocation)) {
						moves.add(new PlayCardAction<UnoLocations>(forPlayerID, card, playerLocation,
								UnoLocations.DISCARDPILE));
					}
				}
			}
		}
		return (List) moves;
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.GameLogic#generateMoves(int, za.ac.sun.cs.ingenious.core.model.GameState)
	 *
	 * Generates all possible  moves for a given state
	 *
	 * @param forPlayerID The player for which moves are generated.
	 * @param fromState Current state.
	 *
	 * @return Returns an ArrayList containing all possible moves.
	 */
	public List<Action> generateActions(UnoGameState fromState, int forPlayerID, Action action) {

		// ArrayList for all possible moves generated.
		ArrayList<PlayCardAction<UnoLocations>> moves = new ArrayList<>();

		// Fetch players location in the location enum.
		UnoLocations playerLocation = UnoLocations.values()[forPlayerID];

		// Fetch card-location-map which.
		TreeMap<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> map = fromState.getMap();

		// Get current Top
		Card<UnoSymbols, UnoSuits> currentTop = fromState.getTop();

		// For each card type
		for (Map.Entry<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> entry : map.entrySet()) {
			Card<UnoSymbols, UnoSuits> card = entry.getKey();

			// If top is not empty and also card type matches top (either suit OR symbol).
			if (currentTop != null && (card.getf1().equals(currentTop.getf1())
					|| card.getf2().equals(currentTop.getf2()))) {

				// Check if player has this card.
				for (UnoLocations location : entry.getValue()) {

					// If so: create a move and add to list.
					if (location.equals(playerLocation)) {
						moves.add(new PlayCardAction<UnoLocations>(forPlayerID, card, playerLocation,
								UnoLocations.DISCARDPILE));
					}
				}

				// If top is empty: every card the player is holding is a possible move.
			} else if (currentTop == null) {
				for (UnoLocations location : entry.getValue()) {
					if (location.equals(playerLocation)) {
						moves.add(new PlayCardAction<UnoLocations>(forPlayerID, card, playerLocation,
								UnoLocations.DISCARDPILE));
					}
				}
			}
		}
		return (List) moves;
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.GameLogic#validMove(za.ac.sun.cs.ingenious.core.Move, za.ac.sun.cs.ingenious.core.model.GameState)
	 * 
	 * Checks if a move is valid
	 * 
	 * @param move Move to check
	 * @param fromState current state
	 * 
	 * @return Returns true if the move is valid.
	 */
	@Override
	public boolean validMove(UnoGameState fromState, Move move) {
		
		// Declare all conditions for a valid move and initialize with false.
		boolean validPlayer = false, sameOldLocation = false, validNewLocation = false, compatibleWithTop = false;
				
		// Fetch card-location-map
		TreeMap<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> map = fromState.getMap();
		
		// It's always allowed to draw a card, if it's the right players turn..
		if(move instanceof DrawCardAction){
			return ((DrawCardAction) move).getPlayerID() == fromState.getCurrentPlayer();
		}
		
		// Assure it's a PlayCardMove
		if(!(move instanceof PlayCardAction)){
			return false;
		}
		PlayCardAction<UnoLocations> pcMove = (PlayCardAction) move;

		// Check if it's the right players turn.
		validPlayer = pcMove.getPlayerID() == fromState.getCurrentPlayer();
		validPlayer = pcMove.getPlayerID() == fromState.getCurrentPlayer() && pcMove.getOldLoc() == UnoLocations.values()[pcMove.getPlayerID()];
		
		// Check if old location of card exists in gameState.
		for (UnoLocations loc : map.get(pcMove.getCard())) {
			if (pcMove.getOldLoc().equals(loc)) {
				sameOldLocation = true;
			}
		}

		// Check if compatible with current top.
		if (fromState.getTop() == null) {
			compatibleWithTop = true;
		} else {
			compatibleWithTop = pcMove.getCard().getf1().equals(fromState.getTop().getf1())
					|| pcMove.getCard().getf2().equals(fromState.getTop().getf2());
		}

		// Check if new location is valid

		// If old location is among the first n UnoLocations (which are
		// players), where n is the number of players in a particular game
		// only DISCARDPILE is a valid new location.
		if (pcMove.getOldLoc().ordinal() < fromState.getNumPlayers()) {
			validNewLocation = pcMove.getNewLoc().equals(UnoLocations.DISCARDPILE);
		}
		// If old locations is DRAWPILE only players (first n UnoLocations) are
		// valid new locations, where n is the number of players in a particular
		// game
		else if (pcMove.getOldLoc().equals(UnoLocations.DRAWPILE)) {
			validNewLocation = pcMove.getNewLoc().ordinal() < fromState.getNumPlayers();
		}
		
		// Return true if all conditions are fulfilled.
		return validPlayer && sameOldLocation && validNewLocation && compatibleWithTop;
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.GameLogic#makeMove(za.ac.sun.cs.ingenious.core.Move, za.ac.sun.cs.ingenious.core.model.GameState)
	 *
	 * @param move the move
	 * @param fromState state to change
	 */
	@Override
	public boolean makeMove(UnoGameState fromState, Move move) {		
		
		// If move is not valid: stand idle and return false.
		// Note: validMove() takes current player into account. Therefore don't setNextPlayer() before validMove is executed!
		if (!validMove(fromState, move)) {
			fromState.incrementRoundNR();
			nextTurn(fromState, move);
			return false;
		}
		
		// Move is valid.
		
		// If move is DrawCardMove: modify game state according to move.
		if (move instanceof DrawCardAction) {
			DrawCardAction<UnoLocations> dcMove = (DrawCardAction) move;
			try {
				fromState.changeLocation(dcMove.getCard(), UnoLocations.DRAWPILE, UnoLocations.values()[dcMove.getPlayerID()]);
				
				//Update players rack size.
				fromState.incrementRackSize(dcMove.getPlayerID());
				
			} catch (KeyNotFoundException | LocationNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		// If move is PlayCardMove: modify game state according to move.
		if (move instanceof PlayCardAction) {
			PlayCardAction<UnoLocations> pcMove = (PlayCardAction) move;
			try {
				fromState.changeLocation(pcMove.getCard(),
						pcMove.getOldLoc(), pcMove.getNewLoc());
				
				// Update players rack size.
				fromState.decrementRackSize(pcMove.getPlayerID());
				
				// Update top of the discard pile.
				fromState.changeTop(pcMove.getCard());
				
			} catch (KeyNotFoundException | LocationNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		// Update round NR and current player.
		fromState.incrementRoundNR();
		nextTurn(fromState, move);
		
		return true;
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.GameLogic#undoMove(za.ac.sun.cs.ingenious.core.Move, za.ac.sun.cs.ingenious.core.model.GameState)
	 * 
	 * Undo a move is unsupported for this game.
	 */
	@Override
	public void undoMove(UnoGameState fromState, Move move) {
		throw new UnsupportedOperationException("Method not implemented");
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.GameLogic#isTerminal(za.ac.sun.cs.ingenious.core.model.GameState)
	 * 
	 * Checks if a given state is terminal.
	 * 
	 * @param state State to check.
	 */
	@Override
	public boolean isTerminal(UnoGameState state) {

		// Fetch card-location-map and create 
		TreeMap<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> map = state.getMap();
		
		// ArrayList to store different locations.
		ArrayList<UnoLocations> list = new ArrayList<>();

		// For each card type
		for (Map.Entry<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> entry : map.entrySet()) {
			
			// For each location of a card type.
			for (UnoLocations location : entry.getValue()) {
				
				// If list doesn't contain location: add it.
				if (!list.contains(location)) {
					list.add(location);
				}
			}
		}

		// Terminate if one player doesn't hold any cards. But keep in mind the
		// 2 extra locations (drawpile, discardpile)
		// Also at least a certain number of rounds must be played. (7 * nrOfPlayers)
		// TODO: Only a temporary ugly solution - see issue 116 - https://bitbucket.org/skroon/ingenious-framework/issues/116/make-unogamelogicisterminal-more-elegant
		return list.size() == state.getNumPlayers() - 1 + 2 && state.getRoundNR() > (state.getInitHandSize() * state.getNumPlayers() -1);
	}
}
