package za.ac.sun.cs.ingenious.games.bomberman.network;

import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;


public class UpdateBombsMessage extends PlayedMoveMessage {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * The ID of the player whos bombs are gonna be triggered, -1 to trigger bombs of all players.
	 */
	private int playerID;
	/** Whether the cradles destroyed by detonating bombs should generate upgrades or not */
	private boolean generateUpgrades;

	
	/**
	 * Indicator message.
	 * When this message is received all bomb timers need to be decreased. Bombs with timer 0 must be detonated.
	 */
	public UpdateBombsMessage(int playerID, boolean generateUpgrades) {
		super((Move) null);
		this.playerID = playerID;
		this.generateUpgrades = generateUpgrades;
	}
	
	/**
	 * Returns the ID of the player whos bombs are gonna be triggered, -1 to trigger bombs of all players.
	 */
	public int getPlayerID() {
		return playerID;
	}

	/**
	 * Returns whether new upgrades should be generated when the bombs are detonated or not. 
	 */
	public boolean generateUpgrades() {
		return generateUpgrades;
	}
}
