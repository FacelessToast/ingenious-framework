package za.ac.sun.cs.ingenious.games.go;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.referee.FullyObservableMovesReferee;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;

import java.io.IOException;

public class GoReferee extends FullyObservableMovesReferee<TurnBasedSquareBoard, GoLogicThreadSafe, GoFinalEvaluator> {

    public GoReferee(MatchSetting match, PlayerRepresentation[] players) throws MissingSettingException, IncorrectSettingTypeException, IOException {
        super(match, players, new TurnBasedSquareBoard(match.getSettingAsInt("boardSize"), 0, 2), new GoLogicThreadSafe(match), new GoFinalEvaluator());
    }

    @Override
    protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
        return new GoInitGameMessage(currentState.getBoardSize());
    }
}
