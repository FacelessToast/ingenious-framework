package za.ac.sun.cs.ingenious.search.mcts.simulation;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.Action;

import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SimulationRandom<S extends GameState> implements SimulationThreadSafe<S> {

    GameLogic<S> logic;
    // Evaluator in general usually evaluates to -1 for loss, 0 for draw, 1 for win (zero sum games).
    MctsGameFinalEvaluator<S> evaluator;
    ActionSensor<S> obs;
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    boolean recordMoves;
    private ThreadLocal<ArrayList<Action>> moves = ThreadLocal.withInitial(ArrayList::new);


    /**
     * Constructor.
     *
     * @param logic     The game logic used during the simulation.
     * @param evaluator The game evaluator used to determine the results of the simulation.
     * @param obs       Object to process actions made by the players during playouts.
     *                  Actions are observed from the point of view of the environment player.
     */
    public SimulationRandom(GameLogic<S> logic, MctsGameFinalEvaluator<S> evaluator, ActionSensor<S> obs, boolean recordMoves) {
        this.logic = logic;
        this.evaluator = evaluator;
        this.obs = obs;
        this.recordMoves = recordMoves;
    }

    /**
     * The method that performs random simulation playout.
     *
     * @param state The game state from which to start the simulation playout.
     * @return The result scores
     */
    public SimulationTuple simulate(S state) {
        moves.get().clear();
        Random randomGen = new Random();
        while (!logic.isTerminal(state)) {
            Set<Integer> playersToAct = logic.getCurrentPlayersToAct(state);
            List<Tuple<Action, Integer>> actionsPlayerIds = new ArrayList<Tuple<Action, Integer>>();

            /**
             * The following is for ordered turn based games, as well as out of order games
             * (ie. any player out of a subset of the players could make the next move,
             * not just one specific person as in ordered turn based games).
             */
            // populate actionsPlayerIds wih all possible actions-playerId pairs that can take place from state
            for (int playerId : playersToAct) {
                List<Action> actions = logic.generateActions(state, playerId);
                for (Action action : actions) {
                    actionsPlayerIds.add(new Tuple<Action, Integer>(action, playerId));
                }
            }

            // if actions are possible, choose one randomly
            if (!actionsPlayerIds.isEmpty()) {
                Tuple<Action, Integer> randomChoice = actionsPlayerIds.get(randomGen.nextInt(actionsPlayerIds.size()));
                Action randomAction = randomChoice.getX();
                logic.makeMove(state, obs.fromPointOfView(randomAction, state, randomChoice.getY()));
                if (recordMoves) {
                    moves.get().add(randomAction);
                }
            } else {
                Log.error("SimulationRandom", "Error during simulation: state is not terminal, however there is also no actions possible");
                Log.error("SimulationRandom", ("PROBLEM STATE: \n" + state + "\nIs PROBLEM STATE terminal? = " + logic.isTerminal(state)));
                break;
            }
        }

        double[] scores = evaluator.getMctsScore(state);
        SimulationTuple returnValues = new SimulationTuple(moves.get(), scores);
        return returnValues;
    }

    /**
     * Game logic object getter.
     *
     * @return The game logic
     */
    public GameLogic<S> getLogic() {
        return logic;
    }

    public ArrayList<Action> getMoves() {
        return moves.get();
    }

    public static <SS extends GameState> SimulationThreadSafe<SS> newSimulationRandom(GameLogic<SS> logic, MctsGameFinalEvaluator<SS> evaluator, ActionSensor<SS> obs, boolean recordMoves) {
        return new SimulationRandom(logic, evaluator, obs, recordMoves);
    }


    /**
     * Helper object - an implementation of a generic tuple object, which stores
     * two object elements in as a tuple.
     */
    private class Tuple<X, Y> {
        X x;
        Y y;

        Tuple(X x, Y y) {
            this.x = x;
            this.y = y;
        }

        X getX() {
            return x;
        }

        Y getY() {
            return y;
        }
    }

}
