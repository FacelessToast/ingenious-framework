package za.ac.sun.cs.ingenious.search.mcts.selection;

import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.PW.MctsPWNodeExtensionParallel;

import java.util.*;

/**
 * A standard implementation of the TreeEngine selection strategy {@link TreeSelectionPW},
 * that calculates UCT values to make a decision of which child node to select.
 * This strategy is specifically for TreeEngine selection and makes use of read locks
 * when viewing node information that is used in the calculations.
 *
 * @author Karen Laubscher
 *
 * @param <S>  The game state type.
 * @param <N>  The type of the mcts node.
 */
public class TreeSelectionPW<S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, ?>> implements TreeSelection<N>{

    private GameLogic<S> logic;
    private static double C;
    private static double alpha;

    /**
     * Constructor to create a TreeEngine UCT selection object, since the constant C
     * is not supplied, the default value for C is assigned as square root of 2.
     *
     * @param logic  The game logic
     */
    private TreeSelectionPW(GameLogic<S> logic) {
        this.logic = logic;
        this.C = 1;
        this.alpha = 0.5;
    }

    /**
     * Constructor to create a TreeEngine UCT selection object with a specified value
     * for the constant C used in the UCT value calculation.
     *
     * @param logic  The game logic
     * @param C      The constant value for C used for the progressive widening algorithm
     * @param alpha      The constant value for alpha (between 0 and 1) used for the progressive widening algorithm
     */
    private TreeSelectionPW(GameLogic<S> logic, double C, double alpha) {
        this.logic = logic;
        this.C = C;
        this.alpha = alpha;
    }


    /**
     * The method that decide which child node to traverse to next, based on
     * calculating the UCT value for each child and then selecting the child
     * with the highest UCT value. Since the TreeEngine sturture is shared in TreeEngine
     * parallelisation, nodes are (read) locked when information for
     * the calculations are viewed.
     *
     * @param node  The current node whose children nodes are considered for the
     *				next node to traverse to.
     *
     * @return      The selected child node with the highest UCT value.
     */
    public N select(N node) {
        //Terminal state - no further selection takes place, therefore return node as selected node

        if (logic.isTerminal(node.getState())) {
            return node;
        }
        List<N> children;
        children = node.getChildren();

        Hashtable<String, MctsNodeExtensionParallelInterface> enhancements = node.getEnhancementClasses();
        node.readLockEnhancementClassesArrayList();
        MctsPWNodeExtensionParallel pwEnhancementExtensionParallel = (MctsPWNodeExtensionParallel)enhancements.get("PW");
        node.readUnlockEnhancementClassesArrayList();

        if (pwEnhancementExtensionParallel.getPrunedChildren() == null) {
            pwEnhancementExtensionParallel.instantiatePrunedChildren(children);
        }
        if (children.size() == 0) {
            return null;
        }

        int k = (int)(C*Math.pow(node.getVisitCount(), alpha));
        // sort the children in descending order of reward value
        pwEnhancementExtensionParallel.setWriteLockPrunedChildred();
        Collections.sort(pwEnhancementExtensionParallel.getPrunedChildrenNoLock(), new SortbyReward());
        pwEnhancementExtensionParallel.unsetWriteLockPrunedChildred();

        pwEnhancementExtensionParallel.setWriteLockPrunedChildred();
        int PrunedChildrenSize = pwEnhancementExtensionParallel.getPrunedChildrenSize();
        int unprunedChildrenSize = pwEnhancementExtensionParallel.getUnprunedChildrenSize();
        if (unprunedChildrenSize < children.size()) {
            // If some children are still pruned, unprune the bandit arms before continuing uct selection on the unpruned arms
            int numberOfArmsToAdd = k - pwEnhancementExtensionParallel.getUnprunedChildrenSize();
            if (numberOfArmsToAdd >= PrunedChildrenSize) {
                // unprune all bandit arms
                for (int i = (PrunedChildrenSize-1); i >= 0; i--) {
                    pwEnhancementExtensionParallel.addChildToUnprunedChildren(pwEnhancementExtensionParallel.getPrunedChild(i));
                    pwEnhancementExtensionParallel.removeChildFromPrunedChildren(i);
                }
            } else {
                for (int i = (PrunedChildrenSize-1); i >= (PrunedChildrenSize-numberOfArmsToAdd); i--) {
                    pwEnhancementExtensionParallel.addChildToUnprunedChildren(pwEnhancementExtensionParallel.getPrunedChild(i));
                    pwEnhancementExtensionParallel.removeChildFromPrunedChildren(i);
                }
            }
        }
        pwEnhancementExtensionParallel.unsetWriteLockPrunedChildred();

        // applies the UCT selection strategy on the subset of k moves used in the progressive widening step
        ArrayList<N> unprunedChildren = pwEnhancementExtensionParallel.getUnprunedChildren();
        double currentVisitCount;
        currentVisitCount = node.getVisitCount();
        N highestUctChild = null;
        double tempVal;
        double highestUct = Double.NEGATIVE_INFINITY;

        for (N child : unprunedChildren) {
            tempVal = child.getValue()/child.getVisitCount() + 100*Math.sqrt((2*Math.log(currentVisitCount)/child.getVisitCount()));
            if (Double.compare(tempVal, highestUct) > 0) {
                highestUctChild = child;
                highestUct = tempVal;
            }
        }

        if (unprunedChildren.size() == 1) {
            highestUctChild = node.getChildren().get(0);
        }
        if (highestUctChild == null || node.getUnexploredChildren().size() != 0) {
            return null;
        }

        highestUctChild.applyVirtualLoss(-10);
        return highestUctChild;
    }

    /**
     * A static factory method, which creates a TreeEngine UCT selection object that
     * uses the default value of square root 2 for the constant C in the UCTS
     * calculation.
     *
     * This factory method is also a generic method, which uses Java type
     * inferencing to encapsulate the complex generic type capturing required
     * by the TreeEngine UCT selection constructor.
     *
     * @param logic   The game logic
     */
    public static <SS extends GameState, NN extends MctsNodeTreeParallelInterface<SS, NN, ?>> TreeSelection<NN> newTreeSelectionProgressiveWidening(GameLogic<SS> logic) {
        return new TreeSelectionPW<>(logic);
    }

    /**
     * A static factory method, which creates a TreeEngine UCT selection object that
     * uses the specified value for the constant C in the UCT value calculation.
     *
     * This factory method is also a generic method, which uses Java type
     * inferencing to encapsulate the complex generic type capturing required
     * by the TreeEngine UCT selection constructor.
     *
     * @param logic   The game logic
     * @param c       The constant value for C (in the UCT calculation).
     */
    public static <SS extends GameState, NN extends MctsNodeTreeParallelInterface<SS, NN, ?>> TreeSelection<NN> newTreeSelectionProgressiveWidening(GameLogic<SS> logic, double c, double alpha) {
        return new TreeSelectionPW<>(logic, c, alpha);
    }

    class SortbyReward implements Comparator<N>
    {
        // Used for sorting in ascending order of
        // roll number
        public int compare(N a, N b)
        {
            return (int)(100*((a.getValue()/a.getVisitCount()) - (b.getValue()/b.getVisitCount())));
        }
    }

}
