package za.ac.sun.cs.ingenious.games.cardGames.uno.game;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.referee.FullyObservableMovesReferee;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.DrawCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.HiddenDrawCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.deck.CardDeck;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.EmptyDrawPileException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSuits;
import za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures.UnoSymbols;

/**
 * The Class UnoReferee.
 * 
 * @author Joshua Wiebe
 */
public class UnoReferee extends FullyObservableMovesReferee<UnoGameState, UnoGameLogic, UnoFinalEvaluator> {
	
	public static final int MAXNUMBEROFPLAYERS = 8;
	
	public static final int MINNUMBEROFPLAYERS = 2;
	
	/**
	 * Instantiates a new Uno referee.
	 *
	 * @param match The match settings
	 * @param players The player representations
	 */
	public UnoReferee(MatchSetting match, PlayerRepresentation[] players) {
		super(match, players, new UnoGameState(new CardDeck<UnoSymbols, UnoSuits>(EnumSet.allOf(UnoSymbols.class), EnumSet.allOf(UnoSuits.class), 2),
				initAndReturnNumPlayers(match)), new UnoGameLogic(), new UnoFinalEvaluator());
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.util.referee.PerfectInformationAlternatingPlayReferee#beforeGameStarts()
	 * 
	 * Before the game starts.
	 */
	@Override
	protected void beforeGameStarts() {
		Log.info();
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.util.referee.PerfectInformationAlternatingPlayReferee#createInitGameAction(za.ac.sun.cs.ingenious.core.PlayerRepresentation)
	 * 
	 * Creates the initGameAction with a new generated rack.
	 * 
	 * @return Returns the initGameAction
	 */
	@Override
	protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
		int numPlayers = currentState.getNumPlayers();
		
		// Create a new rack for given player
		CardRack<UnoSymbols, UnoSuits> rack = createInitRack(player.getID(), currentState);
		
		// Create the InitGameAction
		UnoInitGameMessage iga = new UnoInitGameMessage(rack, player.getID(), numPlayers);
		
		return iga;
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.util.referee.PerfectInformationAlternatingPlayReferee#reactToValidMove(int, za.ac.sun.cs.ingenious.core.network.game.actions.PlayMoveAction)
	 *
	 * This method will be run if a given move was valid.
	 * Prints the most relevant changes made within this turn.
	 * 
	 * @param player Current player
	 * @param pma valid Move
	 */
	@Override
	protected void reactToValidAction(int player, PlayActionMessage pam) {
		StringBuilder s = new StringBuilder();
		int outputWidth = 62;
		
		// Check again if move is null-move.
		if (pam.getAction() != null) {
		
			// Print round NR.
			s.append("\n+-------------------------------------------------------------+\n");
			
			String tmpString = "|                           Round " + (currentState.getRoundNR()-1);
			s.append(tmpString);
			for(int i=0; i<outputWidth-tmpString.length(); i++){
				s.append(" ");
			}
			s.append("|\n");
			
			// Print empty line.
			s.append("|                                                             |\n");
			
			// Print players move (PlayCardMove or DrawCardMove)
			tmpString = "| Player " + player + ": " + pam.getAction().toString();
			s.append(tmpString);
			for(int i=0; i<outputWidth-tmpString.length(); i++){
				s.append(" ");
			}
			s.append("|\n");
			
			// Print updated top if it was a PlayCardMove otherwise top didn't change (DrawCardMove).
			tmpString = "| Top: " + currentState.getTop().toString();
			s.append(tmpString);
			for(int i=0; i<outputWidth-tmpString.length(); i++){
				s.append(" ");
			}
			s.append("|\n");
			
			// Print current rackSizes.
			StringBuilder tmpStringBuilder = new StringBuilder();
			int[] rackSizes = currentState.getRackSizes();
			tmpStringBuilder.append("| Rack sizes: [");
			for(int i=0; i<rackSizes.length; i++){
				if(i==0){tmpStringBuilder.append(rackSizes[i]);}
				else{tmpStringBuilder.append(", " + rackSizes[i]);}
			}
			tmpStringBuilder.append("]");
			s.append(tmpStringBuilder.toString());
			for(int i=0; i<outputWidth-tmpStringBuilder.length(); i++){
				s.append(" ");
			}
			s.append("|\n");
			
			// Print last line.
			s.append("+-------------------------------------------------------------+\n");
			
			// Log.
			Log.info(s);
		}
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.util.referee.PerfectInformationAlternatingPlayReferee#reactToInvalidMove(int, za.ac.sun.cs.ingenious.core.network.game.actions.PlayMoveAction)
	 * 
	 * This method will be run if a player sent in a null-move or an invalid move.
	 * It mainly just makes the player who sent in the invalid move draw a card from the draw pile.
	 * After this, reactToValidMove() will be called with the readjusted move.
	 * 
	 * @param player Current player
	 * @param m Invalid move or null move (reactToNullMove just calls this method)
	 */
	@Override
	protected void reactToInvalidAction(int player, PlayActionMessage m) {
		Card<UnoSymbols, UnoSuits> card = null;
		
		// Draw a card from draw pile
		try {
			card = drawFromDrawPile(player);
		} catch (EmptyDrawPileException e) {
			e.printStackTrace();
		}

		// Create new DrawCardMove
		DrawCardAction<UnoLocations> dca = new DrawCardAction<>(player, card, UnoLocations.DRAWPILE,
				UnoLocations.values()[player]);

		// Update referees game state with DrawCardMove
		logic.makeMove(currentState, dca);

		// Only let the current player know which card he got.
		for(PlayerRepresentation playerRepr : players){
			if(playerRepr.getID() == player){
				PlayedMoveMessage pmm = new PlayedMoveMessage(dca);
				players[player].playMove(pmm);
				
				// Now the move is valid and can be processed to reactToValidMove.
				reactToValidAction(players[player].getID(), new PlayActionMessage(dca));
				
			}
			
			// Distribute DrawCardMove to all other players (HiddenDrawCardMove).
			else{
				PlayedMoveMessage pmm = new PlayedMoveMessage(new HiddenDrawCardAction(player));
				players[playerRepr.getID()].playMove(pmm);
			}
		}
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.util.referee.PerfectInformationAlternatingPlayReferee#reactToNullMove(int, za.ac.sun.cs.ingenious.core.network.game.actions.PlayMoveAction)
	 * 
	 * Call reactToInvalidMove
	 * 
	 * @param player Current player
	 * @param m Null move (of course also invalid) 
	 */
	@Override
	protected void reactToNullAction(int player, PlayActionMessage m) {
		reactToInvalidAction(player, m);
	}
	
	@Override
	protected void afterRound(Map<Integer, PlayActionMessage> messages) {
		// Do nothing - printing the state creates excessive output
	}

	/**
	 * Creates the initial rack for a given player.
	 *
	 * @param player current player
	 * @return the generated card rack
	 */
	protected CardRack<UnoSymbols, UnoSuits> createInitRack(int player, UnoGameState unoGameState) {
		CardRack<UnoSymbols, UnoSuits> rack = new CardRack<>();

		// While rack is not full
		while (rack.size() < unoGameState.getInitHandSize()) {
			
			// draw new card by calling drawFromDrawPile. Then add it to rack.
			try {
				Card<UnoSymbols, UnoSuits> card = drawFromDrawPile(player);
				rack.addCard(card);
				currentState.changeLocation(card, UnoLocations.DRAWPILE, UnoLocations.values()[player]);
			} catch (EmptyDrawPileException | KeyNotFoundException | LocationNotFoundException e) {
				e.printStackTrace();
			}
		}

		// After the rack for a given player is generated successfully, print it.
		StringBuilder s = new StringBuilder();
		
		int width = 17;
		
		s.append("\n+----------------+\n");
		s.append("|Rack of Player " + player + "|\n");
		s.append("+----------------+\n");
		for (Card<UnoSymbols, UnoSuits> card : rack) {
			String tmpString = "|" + card.toString();
			s.append(tmpString);
			for(int i=0; i<width-tmpString.length(); i++){s.append(" ");}
			s.append("|\n");
		}
		s.append("+----------------+\n\n");
		Log.info(s);
		
		return rack;
	}

	/**
	 * Draw from draw pile.
	 *
	 * @param player The drawing player.
	 * @return The drawn card
	 * 
	 * @throws EmptyDrawPileException if the pile is empty.
	 */
	protected Card<UnoSymbols, UnoSuits> drawFromDrawPile(int player) throws EmptyDrawPileException {
		
		// Fetch card-location-map.
		TreeMap<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> map = currentState.getMap();
		
		Random randomizer = new Random();
		Card<UnoSymbols, UnoSuits> card;

		// If draw pile is empty: throw new EmptyDrawPileException (RuntimeException)
		if (currentState.getSizeOfDrawPile() == 0) {
			throw new EmptyDrawPileException("Empty DrawPile!");
		}
		
		// Get all potential cards
		ArrayList<Card<UnoSymbols, UnoSuits>> potentialCards = new ArrayList<>();
		// For each card type
		for (Map.Entry<Card<UnoSymbols, UnoSuits>, ArrayList<UnoLocations>> entry : map.entrySet()) {
			// For each location of that cardType.
			for (UnoLocations location : entry.getValue()) {
				// add if it is on draw pile.
				if(location==UnoLocations.DRAWPILE){
					potentialCards.add(entry.getKey());
				}
			}
		}

		// Select a card randomly
		int rand = randomizer.nextInt(potentialCards.size());
		card = potentialCards.get(rand);

		// Return card.
		return card;
	}
	
	/**
	 * This method checks if the given number of players if valid.
	 * Must be static because it's called before the referee is instantiated (see constructor)
	 * 
	 * @param match A MatchSetting object.
	 * @return The number of players
	 */
	private static int initAndReturnNumPlayers(MatchSetting match){
		int numPlayers = match.getNumPlayers();
		if(numPlayers > MAXNUMBEROFPLAYERS || numPlayers < MINNUMBEROFPLAYERS){
			throw new IllegalArgumentException("===Invalid number of players===");
		}
		return numPlayers;
	}
}