package za.ac.sun.cs.ingenious.games.loa;

import com.esotericsoftware.minlog.Log;

import java.io.IOException;
import java.net.UnknownHostException;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;

public class RandomLOAEngine extends Engine {
	private LOABoard gameBoard;
	private LOALogic logic;

	public RandomLOAEngine(EngineToServerConnection toServer) throws UnknownHostException, IOException {
		super(toServer);
		gameBoard = new LOABoard(new int[][] {
			{2,1,1,1,1,1,1,2},
			{0,2,2,2,2,2,2,0},
			{0,2,2,2,2,2,2,0},
			{0,2,2,2,2,2,2,0},
			{0,2,2,2,2,2,2,0},
			{0,2,2,2,2,2,2,0},
			{0,2,2,2,2,2,2,0},
			{2,1,1,1,1,1,1,2}}, 0);
		this.logic = new LOALogic();
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	@Override
	public String engineName(){
		return "LOA Engine";
	}

	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage a){
		Log.info("playmove received");
		logic.makeMove(gameBoard, a.getMove());
		Log.info(gameBoard);
	}


	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a){
		LOAAction move = (LOAAction)logic.generateActions(gameBoard, -1).get(0);
		return new PlayActionMessage(move);
	}

	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
		// TODO Auto-generated method stub
		
	}



}
