#!/bin/bash

# creates a new Uno game with the default configuration
java -jar ../../../../../../../../../../../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "Uno.json" -game "Uno" -lobby "mylobby" -players 4
